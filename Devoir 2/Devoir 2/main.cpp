﻿/**
* fichier : main.cpp
* auteur : Marie-Hélène Gadbois-Del Carpio - 19091796
* auteur : Marianne Brisson - 19094436
* date : 30 septembre 2019
* description : programme de calcul de population suite à une invasion de zombies
*/

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main()
{
	// Déclaration/Définition des constantes
	const int POPUL_MONTREAL = 1765616;
	const int POPUL_QUEBEC = 585485;
	const int POPUL_SHERBROOKE = 166988;
	const int POPUL_DRUMMONDVILLE = 75771;
	const int POPUL_TORONTO = 6242273;
	const int POPUL_PARIS = 12568755;
	const int POPUL_SHANGHAI = 26317104;

	// Déclaration/Définition des variables
	int populSaine;                             // population courante
	int nbZombies = -1;                         // nb de zombies et l'initialiser à -1 pour que la boucle s'effectue une fois
	int nbJours = 0;                            // nb de jours passés
	int choixVille;                             // choix de la ville
	int populInitiale;                          //Population initiale
	float probMorsure = -1;                     // Probalité d'être mordue
	float propZombiesFaim = -1;                 // Proportion de zombies qui meurent de faim
	float probZombiesTues = -1;                 //Probalité qu'un zombie se fasse tué par un humain
	int nbZombtuesJour = 0;                     //Le nombre de zombies tués par jour
	int nbZombFaimJour = 0;                     //Le nombre de zombies morts de faim par jour
	int nbZombiesMortsJour = 0;                 //Le nombre de zombies morts par jour
	int nbNouvZombies = 0;                      //nb nouveaux zombies

	 // ************** Entrées

	 // Entrer la ville
	cout << "Pour quelle ville voulez-vous verifier l'evolution de la population? " << endl;
	cout << "1 - Montreal (population " << POPUL_MONTREAL << " habitants)" << endl;
	cout << "2 - Quebec (population " << POPUL_QUEBEC << " habitants)" << endl;
	cout << "3 - Sherbooke (population " << POPUL_SHERBROOKE << " habitants)" << endl;
	cout << "4 - Drummondville (population " << POPUL_DRUMMONDVILLE << " habitants)" << endl;
	cout << "5 - Toronto (population " << POPUL_TORONTO << " habitants)" << endl;
	cout << "6 - Paris (population " << POPUL_PARIS << " habitants)" << endl;
	cout << "7 - Shanghai (population " << POPUL_SHANGHAI << " habitants)" << endl;

	//Savoir quelle ville a été choisie
	cout << "Veuillez choisir une ville(1 a 7) : ";
	cin >> choixVille;
	while (choixVille < 1 && choixVille>7)
	{
		cout << "Veuillez choisir une ville(1 a 7) : ";
		cin >> choixVille;
	}

	if (choixVille == 1)
	{
		populInitiale = POPUL_MONTREAL;
	}
	else if (choixVille == 2)
	{
		populInitiale = POPUL_QUEBEC;
	}
	else if (choixVille == 3)
	{
		populInitiale = POPUL_SHERBROOKE;
	}
	else if (choixVille == 4)
	{
		populInitiale = POPUL_DRUMMONDVILLE;
	}
	else if (choixVille == 5)
	{
		populInitiale = POPUL_TORONTO;
	}
	else if (choixVille == 6)
	{
		populInitiale = POPUL_PARIS;
	}
	else
	{
		populInitiale = POPUL_SHANGHAI;
	}


	//Le nombre de personnes saines
	populSaine = populInitiale;

	//Entrer le nombre de zombies au depart
	while (nbZombies < 0)
	{
		cout << "Entrer le nombre de zombies: ";
		cin >> nbZombies;
		cout << endl;
	}


	//Entrer la probabilité qu’une personne saine soit mordue si elle rencontre un zombie

	while (probMorsure < 0 || probMorsure > 1)
	{
		cout << "Entrer la probabilite	qu une personne saine soit mordue si elle rencontre un zombie: ";
		cin >> probMorsure;
	}


	//Entrer la proportion de zombies qui meurent de faim
	cout << "Entrer la proportion de zombies qui meurent de faim: ";
	cin >> propZombiesFaim;
	while (propZombiesFaim < 0 || propZombiesFaim > 1)
	{
		cout << "Entrer la proportion de zombies qui meurent de faim: ";
		cin >> propZombiesFaim;
	}

	//Entrer la probabilité qu’un zombie se fasse tuer si elle rencontre une personne saine
	cout << "la probabilite qu un zombie se fasse tuer si elle rencontre une personne saine: ";
	cin >> probZombiesTues;
	while (probZombiesTues < 0 || probZombiesTues > 1)
	{
		cout << "la probabilite qu un zombie se fasse tuer si elle rencontre une personne saine: ";
		cin >> probZombiesTues;
	}

	// Début du traitement
// écrire l'état de la population initiale
	cout << "Population au depart      : "
		<< setw(9) << populSaine << ",   zombies : " << setw(9) << nbZombies << endl;

	//Formules
	do
	{

		nbJours = nbJours + 1;

		//nombre de nouveaux zombies
		nbNouvZombies = floor((probMorsure * (nbZombies * populSaine) / populInitiale) + 0.5);

		if (nbNouvZombies > populSaine)
		{
			nbNouvZombies = populSaine;

		}

		//Le nombre de zombies morts de faim par jour
		nbZombFaimJour = floor((propZombiesFaim * nbZombies) + 0.5);

		//Le nombre de zombies tués par jour
		nbZombtuesJour = floor((probZombiesTues * (nbZombies * populSaine) / populInitiale) + 0.5);

		//Nombre de zombie mort
		nbZombiesMortsJour = nbZombtuesJour + nbZombFaimJour;

		//Mise à jour de la population de zombie
		nbZombies = nbZombies - nbZombiesMortsJour + nbNouvZombies;

		//Mise à jour de la population saine
		populSaine = populSaine - nbZombies;


		// écrire l'état des populations
		cout << "Population apres jour " << setw(4) << nbJours << ": "
			<< setw(9) << populSaine << ",   zombies : " << setw(9) << nbZombies << endl;

	} while (nbNouvZombies > 0 && populSaine > 0);
	// Il n'y a plus personne de vivant => nb de jours
	if (populSaine == 0)
	{
		cout << "******** Il n'y a plus de personne vivante apres " << nbJours << " jours *******" << endl << endl;
	}
	else
		// nombre de personnes survivantes => nb de jours
	{
		cout << "******** Apres " << nbJours << " jours, " << populSaine << " personnes ont reussi a eviter d'etre mordues *******" << endl << endl;
	}


	// aucun zombie => nb de jours
	if (nbZombFaimJour > 0 && nbZombFaimJour < 1)
	{
		nbZombFaimJour = 1;
	}
	else
	{
		nbZombFaimJour = floor(nbZombFaimJour + 0.5);
	}



	while (nbZombies > 0)
	{
		nbJours = nbJours + 1;

		if (nbZombFaimJour > nbZombies)
		{
			nbZombFaimJour = nbZombies;
		}

		nbZombies = nbZombies - nbZombFaimJour;
		cout << "Population des zombies apres jour " << setw(4) << nbJours << ": "
			<< setw(9) << nbZombies << endl;
	}

	// aucun zombie => nb de jours
	cout << "******** Il n'y a plus de zombie apres " << nbJours << " jours *******" << endl;

}
